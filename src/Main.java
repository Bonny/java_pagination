import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) throws SQLException {

		Connection c = null;

		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/smudb147", "smu", "elaidemo1");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Opened database successfully");

		Statement stmt = c.createStatement();
		ResultSet rs;

		rs = stmt.executeQuery("SELECT id FROM SMU_MONITOR;");
		while (rs.next())
			System.out.println("id = " + rs.getBigDecimal("id"));

		rs = stmt.executeQuery("SELECT COUNT(*) AS totalRows FROM SMU_MONITOR;");
		rs.next();

		final int pageSize = 10;
		int totalRows = rs.getInt("totalRows");
		int totalPages = (int) Math.ceil(totalRows / pageSize);

		System.out.println("totalRows = " + totalRows);
		System.out.println("totalPages = " + totalPages);
		System.out.println("pageSize = " + pageSize);

		String query;
		int offset = 0;
		int currentPage = 0;

		for (; currentPage <= totalPages; currentPage++) {

			System.out.println();
			System.out.println("currentPage = " + (currentPage + 1));

			offset = currentPage * pageSize;

			query = String.format("SELECT id FROM SMU_MONITOR LIMIT %d OFFSET %d;", pageSize, offset);
			System.out.println("query = " + query);

			rs = stmt.executeQuery(query);

			while (rs.next())
				System.out.println("id = " + rs.getBigDecimal("id"));
		}

		c.close();

	}

}
